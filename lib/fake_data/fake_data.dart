import 'package:flutter/material.dart';
import 'package:lab5_tiki/module/category.dart';
import 'package:lab5_tiki/module/product.dart';

const FAKE_CATEGORIES = const [
  Category(1, "THỜI TRANG NAM", "https://cdn.tgdd.vn/Files/2021/01/29/1323895/top-11-thuong-hieu-thoi-trang-nam-noi-tieng-tai-viet-nam-ma-nam-gioi-can-biet-202101291507373398.jpg"),
  Category(2, "THỜI TRANG NỮ", "https://stylecaster.com/wp-content/uploads/2021/08/dresss.jpg"),
  Category(3, "ĐIỆN TỬ", "https://www.theman.in/content/dam/theman/gadgets/images/2019/phone-use-main.jpg"),
  Category(4, "SỨC KHỎE", "https://i.pinimg.com/originals/73/66/ca/7366cafe876be57426ffecd8c57a5caf.jpg"),
];

