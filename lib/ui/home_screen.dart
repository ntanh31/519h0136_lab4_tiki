import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      color: Colors.greenAccent,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Freeship", style: TextStyle(fontStyle: FontStyle.italic, fontSize: 25, fontWeight: FontWeight.bold, color: Colors.white),),
              SizedBox(width: 30,),
              Text("TIKI", style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold, color: Colors.white),),
              SizedBox(width: 30,),
              IconButton(
                onPressed: (){},
                icon: Icon(Icons.notifications_none_rounded),
                iconSize: 30,
              ),
              IconButton(
                  onPressed: (){},
                  icon: Icon(Icons.shopping_cart_outlined),
                iconSize: 30,
              ),
            ],
          ),
          SizedBox(height: 20,),
          CupertinoSearchTextField(
            backgroundColor: Colors.white,
          ),

        ],
      ),
    );
  }
}
