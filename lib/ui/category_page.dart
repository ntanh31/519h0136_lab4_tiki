import 'package:flutter/material.dart';
import 'package:lab5_tiki/fake_data/fake_data.dart';
import 'package:lab5_tiki/ui/category_item.dart';

class CategoryPage extends StatefulWidget {
  const CategoryPage({Key? key}) : super(key: key);

  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      color: Colors.greenAccent,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Freeship", style: TextStyle(fontStyle: FontStyle.italic, fontSize: 25, fontWeight: FontWeight.bold, color: Colors.white),),
              SizedBox(width: 30,),
              Text("TIKI", style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold, color: Colors.white),),
              SizedBox(width: 30,),
              IconButton(
                onPressed: (){},
                icon: Icon(Icons.notifications_none_rounded),
                iconSize: 30,
              ),
              IconButton(
                onPressed: (){},
                icon: Icon(Icons.shopping_cart_outlined),
                iconSize: 30,
              ),
            ],
          ),
          GridView(
            padding: EdgeInsets.all(15),
            shrinkWrap: true,
            children: FAKE_CATEGORIES.map((e) => CategoryItem(e)).toList(),
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 300,
              childAspectRatio: 3/3,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
            ),
          )
        ],
      ),
    );
  }
}
